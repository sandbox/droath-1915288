<?php
/**
 * @file
 * The tabilicious template for a tab style plugin.
 */
?>

<div <?php print $attributes; ?> class="<?php print $classes; ?>">

  <!-- Render the tab links -->
  <ul class="tabs">
    <?php foreach($links as $link): ?>
      <li class="tab">
        <?php print drupal_render($link); ?>
      </li>
    <?php endforeach; ?>
  </ul>

  <!-- Render the tab contents -->
  <?php foreach($contents as $content): ?>
    <?php print drupal_render($content); ?>
  <?php endforeach; ?>

</div>
