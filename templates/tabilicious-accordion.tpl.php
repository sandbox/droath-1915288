<?php
/**
 * @file
 * The tabilicious template for a accordion style plugin.
 */
?>

<div <?php print $attributes; ?> class="<?php print $classes; ?>">

  <!-- Render the accordion contents -->
  <?php foreach($contents as $content): ?>
    <?php print drupal_render($content); ?>
  <?php endforeach; ?>

</div>
