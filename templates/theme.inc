<?php

/**
 * @file
 * The theme implementations for the tabilicious module.
 */

/**
 * Template preprocess for the tabilicious jQuery tabs.
 *
 * @param $variables
 *   An array of the element variables.
 */
function template_preprocess_tabilicious_tabs(&$variables) {
  $variables['links']    = array();
  $variables['contents'] = array();

  $element  = $variables['element'];
  $selector = $element['#selector'];

  $variables['attributes_array'] = array(
    'id' => tabilicious_html_id($selector . '-tabilicious'),
  );
  $variables['classes_array'][] = 'tabilicious';
  $variables['classes_array'][] = drupal_html_class($selector . '-tabilicious');

  // Iterate through the element items.
  foreach ($element['#items'] as $delta => $item) {
    $id = tabilicious_html_id($item['title'] . '-' . $delta);

    // Build the tab links array.
    $variables['links'][] = array(
      '#theme' => 'link',
      '#text' => $item['title'],
      '#path' => NULL,
      '#options' => array(
        'html' => FALSE,
        'fragment' => $id,
        'external' => TRUE,
        'attributes' => array(),
      ),
    );

    // Build the tab content array.
    $variables['contents'][] = array(
      '#theme' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => array(
        'id' => $id,
        'class' => array('tab-content'),
      ),
      '#value' => check_markup($item['content'], (isset($item['format']) ? $item['format'] : NULL)),
    );
  }
}

/**
 * Template preprocess for the tabilicious jQuery accordion.
 *
 * @param $variables
 *   An array of the element variables.
 */
function template_preprocess_tabilicious_accordion(&$variables) {
  $variables['contents'] = array();

  $element  = $variables['element'];
  $selector = $element['#selector'];

  $variables['attributes_array'] = array(
    'id' => tabilicious_html_id($selector . '-tabilicious'),
  );
  $variables['classes_array'][] = 'tabilicious';
  $variables['classes_array'][] = drupal_html_class($selector . '-tabilicious');

  // Iterate through the element items.
  foreach ($element['#items'] as $delta => $item) {
    $id = tabilicious_html_id($item['title'] . '-' . $delta);

    // Build the accordion content array.
    $variables['contents'][] = array(
      'title' => array(
        '#theme' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => array(
          'class' => array('accordion-title'),
        ),
        '#value' => l($item['title'], NULL, array(
          'html' => FALSE,
          'fragment' => $id,
          'external' => TRUE,
          'attributes' => array(),
          )
        ),
      ),
      'content' => array(
        '#theme' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => array(
          'id' => $id,
          'class' => array('accordion-content'),
        ),
        '#value' => check_markup($item['content'], (isset($item['format']) ? $item['format'] : NULL)),
      ),
    );
  }
}

/**
 * Theme function for the tabilicious overview list.
 *
 * @param $variables
 *   An array of the form element.
 *
 * @return sting
 *   The render markup of the overview list.
 */
function theme_tabilicious_build_overview_list(&$variables) {
  $form = $variables['form'];

  $header = array(
    'label' => t('Label'),
    'name' => t('Name'),
    'style' => t('Style'),
    'operations' => t('Operations'),
  );
  $rows = array();

  // Iterate through the tabilicious profiles.
  foreach (element_children($form['profiles']) as $name) {

    // Build the rows for all the profiles.
    $row = array();
    foreach ($form['profiles'][$name]['#info'] as $result) {
      $row[] = check_plain($result);
    }

    // Build the operational links for each profile.
    $operational_links = array();
    foreach (tabilicious_profile_operations() as $op => $label) {
      $operational_links['links'][$op] = array(
        'title' => $label,
        'href' => 'admin/config/content/tabilicious/' . $op . '/' . $name,
      );
    }
    $row[] = theme('links__ctools_dropbutton', $operational_links);

    // Set each profile row to the rows array.
    $rows[$name] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(
      array('data' => t('There are no profiles created at this point!'), 'colspan' => 4),
    );
  }

  // Render the additional form elements.
  $rows['additional'] = array();
  foreach (array('label', 'name', 'style', 'add') as $element) {
    $rows['additional'][] = drupal_render($form[$element]);
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);

  return $output;
}
