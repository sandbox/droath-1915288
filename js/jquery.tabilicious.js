(function ($) {
  Drupal.behaviors.tabilicious = {
    attach: function(context, settings) {
      var data = settings.tabilicious.data;
      $.each(data, function(selector, value) {
        $('#' + selector + '-tabilicious')[value.method](value.parameters);
      })
    }
  }
})(jQuery);
