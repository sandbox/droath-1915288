<?php

/**
 * @file
 * The administration implementations for the tabilicious module.
 */

/**
 * A callback to check if the machine name exist already.
 */
function tabilicious_profile_exists($name, $element, $form_state) {
  return tabilicious_profile_exist_by_name($name);
}

/**
 * Display the overview list of the created profiles.
 */
function tabilicious_build_overview_list($table, &$form_state) {

  // Iterate through the available profiles.
  foreach (tabilicious_get_profiles() as $name => $profile) {
    $table['profiles'][$name]['#info'] = array(
      'label' => $profile->label,
      'name'  => $profile->name,
      'style' => $profile->style,
    );
  }

  // Define the add new profile form elements.
  $table['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Add new profile'),
    '#required' => TRUE,
  );
  $table['name'] = array(
    '#type' => 'machine_name',
    '#machine_name' => array(
      'exists' => 'tabilicious_profile_exists',
      'source' => array('label'),
      'standalone' => TRUE,
    ),
  );
  $table['style'] = array(
    '#type' => 'select',
    '#options' => tabilicious_build_plugins_options('styles'),
    '#required' => TRUE,
  );
  $table['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );

  return $table;
}

/**
 * The submission handler for the overview list.
 */
function tabilicious_build_overview_list_submit($form, &$form_state) {
  $values = $form_state['values'];

  $query = array(
    'label' => $values['label'],
    'name' => $values['name'],
  );
  $path = tabilicious_profile_base_path() . '/add/' . $values['style'];

  // Redirect to the tabilicious profile operation form.
  $form_state['redirect'] = array('path' => $path, 'options' => array('query' => $query));
}

/**
 * The tabilicious profile import form.
 */
function tabilicious_profile_import($form, &$form_state) {
  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Export code'),
    '#description' => t('Paste in the tabilicious profile export code in the textarea above.'),
    '#rows' => 25,
    '#required' => TRUE,
  );
  $form['import'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * The validation handler for the tabilicious profile import form.
 */
function tabilicious_profile_import_validate($form, &$form_state) {
  $values = $form_state['values'];

  ctools_include('export');
  $object = ctools_export_crud_import('tabilicious_profile', $values['code']);

  // Make sure the exported code has been converted into an object.
  if (empty($object) || !is_object($object)) {
    form_error($form['code'], t('The tabilicious profile object was not created.
     Please export the tabilicious profile and retry importing it.'));
  }
  // Make sure the profile doesn't exist before we save it.
  elseif (tabilicious_profile_exist_by_name($object->name)) {
    form_error($form['code'], t('The tabilicious profile already exist.'));
  }

  // Set the $form_state value to the converted object.
  form_set_value($form['code'], $object, $form_state);
}

/**
 * The submission handler for the tabilicious profile import form.
 */
function tabilicious_profile_import_submit($form, &$form_state) {
  $values = $form_state['values'];
  $object = $values['code'];

  // Save the imported tabilicious profile object.
  tabilicious_profile_save($object);

  // Display the status message based on the operation performed.
  tabilicious_profile_operation_set_message('import');

  $form_state['redirect'] = array('path' => tabilicious_profile_base_path());
}

/**
 * The tabilicious profile default items.
 */
function tabilicious_profile_default_items() {
  return array(
    'name' => NULL,
    'label' => NULL,
    'style' => NULL,
    'settings' => array(),
  );
}

/**
 * Route the tabilicious profile operation form.
 */
function tabilicious_profile_route_operation_form($op, $object) {
  ctools_include('export');
  $schema = ctools_export_get_schema('tabilicious_profile');

  $items = array();
  foreach ($schema['fields'] as $field_name => $info) {
    if (!isset($object->{$field_name})) {
      continue;
    }
    $items[$field_name] = $object->{$field_name};
  }
  $items += tabilicious_profile_default_items();

  // The object needs to be apart of the $form_state.
  $form_state = array(
    'op' => $op,
    'items' => $items,
    'object' => $object,
  );

  return drupal_build_form('tabilicious_profile_operation_form', $form_state);
}

/**
 * Tabilicious profile operations form (add, edit, export, delete).
 */
function tabilicious_profile_operation_form($form, &$form_state) {
  $op     = $form_state['op'];
  $items  = $form_state['items'];
  $object = $form_state['object'];

  // Determine the action to invoke based on the operation.
  if ($op == 'delete') {
    // We are going to stick with the default menu title, so we passed in NULL
    // for the question parameter.
    return confirm_form($form, NULL, tabilicious_profile_base_path());
  }
  elseif ($op == 'export') {
    ctools_include('export');
    $code = ctools_export_object('tabilicious_profile', $object);
    return ctools_export_form($form, $form_state, $code);
  }

  // Anything beyond this point deals with both add or edit operations.
  $path = drupal_get_path('module', 'tabilicious');
  drupal_add_css($path . '/css/' . 'tabilicious-vertical-tabs.css');

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => ($op == 'add') ? $_GET['label'] : $items['label'],
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Name'),
    '#machine_name' => array(
      'exists' => 'tabilicious_profile_exists',
      'source' => array('label'),
    ),
    '#maxlength' => 255,
    '#disabled' => ($op != 'add') ? TRUE : FALSE,
    '#default_value' => ($op == 'add') ? $_GET['name'] : $items['name'],
  );

  // Display the style plugin settings form.
  tabilicious_build_style_settings_form($form, $form_state);

  $form['style'] = array(
    '#type' => 'hidden',
    '#value' => ($op == 'add') ? $object['name'] : $items['style'],
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // Only display these form buttons when we have a profile available.
  if ($op == 'edit') {
    $form['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset Settings'),
      '#action' => 'reset',
      '#limit_validation_errors' => array(),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('tabilicious_profile_operation_form_delete'),
    );
  }

  return $form;
}

/**
 * Build tabilicious style settings form.
 */
function tabilicious_build_style_settings_form(&$form, &$form_state) {
  $items  = $form_state['items'];
  $object = $form_state['object'];

  // Build the the settings array that we'll send to the style plugin.
  $settings = isset($form_state['items']['settings']) ?
    $form_state['items']['settings'] : array();

  // Invoke the tabilicious style plugin build callback. This allows the style
  // plugin to build the setting form. This is only invoked on both add and edit
  // operations.
  $plugin = tabilicious_get_style_by_name(($form_state['op'] == 'add') ? $object['name'] : $items['style']);

  return tabilicious_call_plugins_method($plugin, 'buildSettingsForm',
    array(&$form, &$form_state, $settings));
}

/**
 * The validation handler for the tabilicious profile operation page.
 */
function tabilicious_profile_operation_form_validate($form, &$form_state) {
  if (!isset($form_state['object']) && !isset($form_state['items'])) {
    return;
  }
  $items  = $form_state['items'];
  $object = $form_state['object'];
  $values = $form_state['values'];

  // Build the the settings array that we'll send to the style plugin.
  $settings = isset($values['settings']) ? $values['settings'] : array();

  // Invoke the tabilicious style plugin operation validation callback. This
  // allows the style plugin to react on the validation on all operations being
  // performed.
  $plugin = tabilicious_get_style_by_name(($form_state['op'] == 'add') ? $object['name'] : $items['style']);
  tabilicious_call_plugins_method($plugin, 'validateOperationForm', array($form_state, $settings));
}

/**
 * Display a message based on the operation.
 */
function tabilicious_profile_operation_set_message($op) {
  $base = 'The profile has been successfully';
  $messages = array(
    'add'    => t('@base added!', array('@base' => $base)),
    'edit'   => t('@base updated!', array('@base' => $base)),
    'reset'  => t('@base reset!', array('@base' => $base)),
    'delete' => t('@base deleted!', array('@base' => $base)),
    'import' => t('@base imported!', array('@base' => $base)),
  );

  drupal_set_message($messages[$op]);
}

/**
 * The submission handler for the tabilicious profile operation page.
 */
function tabilicious_profile_operation_form_submit($form, &$form_state) {
  if (!isset($form_state['object']) && !isset($form_state['items'])) {
    return;
  }
  $op     = $form_state['op'];
  $items  = $form_state['items'];
  $object = $form_state['object'];

  // Allow the triggering element action to alter the operation.
  $triggering_element = $form_state['triggering_element'];
  if (isset($triggering_element['#action']) && !empty($triggering_element['#action'])) {
    $op = $triggering_element['#action'];
  }
  $values = $form_state['values'];

  // Determine the action to invoke based on the operation.
  switch ($op) {
    case 'add':
    case 'edit':
      $records = array();
      // Iterate through the items and add or update the records value.
      foreach ($form_state['items'] as $name => $info) {
        $records[$name] = $values[$name];
      }
      if ($op == 'add') {
        $records += array(
          'export_type' => NULL,
        );
      }
      else {
        $records += (array) $object;
      }
      // Save the profile records object.
      tabilicious_profile_save((object) $records);
      break;

    case 'reset':
      // Zero out the settings and re-save the object.
      $object->settings = array();
      tabilicious_profile_save((object) $object);
      break;

    case 'delete':
      tabilicious_profile_delete($object);
      break;
  }
  // Display the status message based on the operation performed.
  tabilicious_profile_operation_set_message($op);

  // Build the the settings array that we'll send to the style plugin.
  $settings = isset($values['settings']) ? $values['settings'] : array();

  // Invoke the tabilicious style plugin operation submission callback. This
  // allows the style plugin to react on the submit on all operations being
  // performed.
  $plugin = tabilicious_get_style_by_name(($op == 'add') ? $object['name'] : $items['style']);
  tabilicious_call_plugins_method($plugin, 'submitOperationForm', array($form_state, $settings));

  if ($op != 'reset') {
    $form_state['redirect'] = array('path' => tabilicious_profile_base_path());
  }
}

/**
 * The deletion handler for the tabilicious profile operation page.
 */
function tabilicious_profile_operation_form_delete($form, &$form_state) {
  $path = tabilicious_profile_base_path() . '/delete/' . $form_state['object']->name;
  $form_state['redirect'] = array('path' => $path);
}
