
-- SUMMARY --

There are many jQuery tab and accordion libraries out these days! How do you
choose which one to implement? Many site builders don't have a programing
background and can get lost when trying to add such libraries to their theme.
Never the less, embedding these libraries into a custom theme will prevent
anyone from easily reusing or customizing the library configurations.

So why another tab or accordion module?
New jQuery libraries are releasing rapidly. Instead of developing a new module
for every tab or accordion library, I created tabilicious! Tabilicious was
designed to be flexible, so developers can easily implement the new libraries
without having to reinvent the wheel.

What libraries does tabilicious support?
At the moment tabilicious works with both jQuery UI Tabs and Accordion.

I plan on releasing a new tabilicious style called "SlideTabs" in the near
future (unfortunately is a paid jQuery plugin).

How do I display my tabilicious profile?
Currently you can render a tabilicious profile by creating a field for any
entity type (node, user, bean, etc). Then configure the field formatter to
render as a "Tabilicious Profile".

I have plans on releasing more options in the future, so you can render
tabilicious profiles using: field group, views, or panels.

Features:
 - Display content in a tab or accordion for any entity type.
 - Export and import tabilicious profile configurations.
 - Manage and customize tab and accordion configurations from a UI.
 - Support other jQuery libraries (not just jQuery UI Tabs and Accordion).

Future Plans:
 - Render tabilicious profile using the panels module.
 - Render tabilicious profile using the views module.
 - Render tabilicious profile using the field group module.
 - Develop a tabilicious style using the jQuery SlideTabs plugin.

Similar Modules:
 - jQuery Tabs Field

-- REQUIREMENTS --

 1. Chaos tool suite (ctools)
 2. Libraries API 2.x

-- INSTALLATION --

 1. Place the entire tabilicious directory into your Drupal sites modules
 directory.
 2. Enable the tabilicious module by navigating to: Administration > Modules.

-- CONFIGURATION --

  First, you need to create a custom tabilicious profile. You will need to
  navigate to: Administration > Configuration > Content authoring >
  Tabilicious profiles.

    1. Input the tabilicious profile label.
    2. Select the desired style.
    3. Click the Add button.

    You are going to be redirected to another page where you can configure
    the tabilicious profile settings.

    4. Select the desired settings for the tabilicious profile.
    5. Click the Save button.

  Next, you will need to determine how you want to render the tabilicious
  profile (more options in the future).

  - Tabilicious Field -

    Display the tabilicious profile on a given entity type (node, user,
    taxonomy, etc).

    1. Enable the tabilicious field module by navigating to:
    Administration > Modules.
    2. Add new field and select "Tabilicious" for Field type.
    3. Configure the field settings.
    4. Click the Save button.

    Now you need to configure Drupal so it knows how to render the new field.

    1. Navigate to the fields Manage Display: Administration > Structure >
    Content types > BUNDLE_NAME.
    2. Select "Tabilicous Profile" as the formatter.
    3. Click the gear on the right.
    4. Choose a tabilicious profile from the dropdown.
    5. Click the Update button.
    6. Click the Save button.

    Now you are ready to add content.

    1. Go to the "add node page" for the content type you created the field on.
    2. Add some content to the node.
    3. Click the Save button.

-- CONTACT --

Current maintainers:
* Travis Tomka (droath) - https://drupal.org/user/718562
