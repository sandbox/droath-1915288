<?php

/**
 * @file
 * The jQuery accordion style implementation for the tabilicious module.
 */

$plugin = array(
  'label' => t('jQuery Accordion'),
  'name' => 'jquery_accordion',
  'module' => 'tabilicious',
  // Define the handlers.
  'handler' => array(
    'js' => 'accordion',
    'class' => 'TabiliciousStyle',
  ),
  // Define the assets that need to be loaded. The assets will be loaded in
  // the same order they are listed below.
  'assets' => array(
    'system' => array(
      'ui.accordion',
    ),
  ),
  // Define the field callbacks.
  'field' => array(
    'parameter' => 'tabilicious_jquery_accordion_parameter_fields',
  ),
  // Define the theme template.
  'template' => 'tabilicious_accordion',
);

/**
 * Define the jQuery accordion parameter fields.
 */
function tabilicious_jquery_accordion_parameter_fields() {
  $fields['active'] = array(
    'group' => 'general',
    'type' => 'integer',
    'title' => t('Active'),
    'options' => tabilicious_get_integer_options(0, 15),
    'empty_option' => t('-None-'),
    'empty_value' => 'false',
    'description' => t('Which panel is currently open.'),
    'default_value' => 0,
  );
  $fields['collapsible'] = array(
    'group' => 'general',
    'type' => 'boolean',
    'title' => t('Collapsible'),
    'description' => t('Whether all the sections can be closed at once.'),
    'default_value' => 0,
  );
  $fields['disabled'] = array(
    'group' => 'general',
    'type' => 'boolean',
    'title' => t('Disabled'),
    'description' => t('Disables the accordion if set to true.'),
    'default_value' => 0,
  );
  $fields['event'] = array(
    'group' => 'general',
    'type' => 'string',
    'title' => t('Event'),
    'options' => array(
      'click' => t('Click'),
      'mouseover' => t('Mouseover'),
    ),
    'description' => t('The event that accordion headers will react to in order
      <br/> to activate the associated panel. Multiple events can be specificed,
      separated by a space.'),
    'default_value' => 'click',
  );
  $fields['autoHeight'] = array(
    'group' => 'content',
    'type' => 'boolean',
    'title' => t('Auto Height'),
    'description' => t('Whether all panels should be set to the height of the
      tallest panel.'),
    'default_value' => 1,
  );
  $fields['clearStyle'] = array(
    'group' => 'content',
    'type' => 'boolean',
    'title' => t('Clear Style'),
    'description' => t('Whether to clear height and overflow styles after
      finishing animations. <br/>This enabled accordions to work with dynamic
      content. Requires the autoHeight option to be false.'),
    'default_value' => 0,
  );
  $fields['fillSpace'] = array(
    'group' => 'content',
    'type' => 'boolean',
    'title' => t('Fill Space'),
    'description' => t('Whether the accordion should expand to the available
      height based on the accordions parent height. <br/> This option will
      override the autoHeight option.'),
    'default_value' => 0,
  );
  $fields['animated'] = array(
    'group' => 'animation',
    'type' => 'string',
    'title' => t('Animated'),
    'description' => t('Animate changing panels.'),
    'options' => array(
      'slide' => t('slide'),
      'bounceslide' => t('bounceslide'),
    ) + tabilicious_get_easing_options(),
    'default_value' => 'slide',
  );

  return $fields;
}
