<?php

/**
 * @file
 * The jQuery tab style implementation for the tabilicious module.
 */

$plugin = array(
  'label' => t('jQuery Tab'),
  'name' => 'jquery_tabs',
  'module' => 'tabilicious',
  // Define the handlers.
  'handler' => array(
    'js' => 'tabs',
    'class' => 'TabiliciousStyle',
  ),
  // Define the assets that need to be loaded. The assets will be loaded in
  // the same order they are listed below.
  'assets' => array(
    'system' => array(
      'ui.tabs',
    ),
  ),
  // Define the field callbacks.
  'field' => array(
    'parameter' => 'tabilicious_jquery_tabs_parameter_fields',
  ),
);

/**
 * Define the jQuery tabs parameter fields.
 */
function tabilicious_jquery_tabs_parameter_fields() {
  $fields['selected'] = array(
    'group' => 'general',
    'type' => 'integer',
    'title' => t('Selected'),
    'options' => tabilicious_get_integer_options(0, 15),
    'empty_option' => t('-None-'),
    'empty_value' => 'false',
    'description' => t('Which panel is currently open.'),
    'default_value' => 0,
  );
  $fields['collapsible'] = array(
    'group' => 'general',
    'type' => 'boolean',
    'title' => t('Collapsible'),
    'description' => t('When set to true, the active panel can be closed.'),
    'default_value' => 0,
  );
  $fields['event'] = array(
    'group' => 'general',
    'type' => 'string',
    'title' => t('Event'),
    'options' => array(
      'click' => t('Click'),
      'mouseover' => t('Mouseover'),
    ),
    'description' => t('The type of event that the tabs should react to in order
      to activate the tab.'),
    'default_value' => 'click',
  );

  return $fields;
}
