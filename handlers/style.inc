<?php

/**
 * @file
 * The tabilicious style plugin base class.
 */

class TabiliciousStyle {

  protected $plugin;

  /**
   * Tabilicious style constructor.
   *
   * @param $plugin
   *   A tabilicious style plugin.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * A build callback for the style plugins settings form.
   *
   * @param $form
   *   An array of the form elements.
   * @param $form_state
   *   An array of the form states.
   * @param $settings
   *   An array of the style plugin settings.
   */
  public function buildSettingsForm(&$form, $form_state, $settings) {
    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('@plugin_label settings', array('@plugin_label' => $this->plugin['label'])),
      '#description' => isset($this->plugin['description']) ? $this->plugin['description'] : NULL,
      '#tree' => TRUE,
    );

    // Iterate through the style plugin categories. By default tabilicious has
    // two categories (parameters, theming). Other third-party style plugins can
    // define their own by extending the plugin method.
    foreach ($this->categories() as $type => $category) {
      $form['settings'][$type] = array(
        '#type' => 'fieldset',
        '#title' => $category['title'],
        '#collapsible' => TRUE,
        '#collapsed' => isset($category['collapsed']) ? $category['collapsed'] : FALSE,
      );
      $base_parents = array($type);

      // Determine if the category has vertical tabs enabled.
      $settings_form = &$form['settings'][$type];
      if (isset($category['vertical_tabs']) && $category['vertical_tabs']) {
        $base_parents = array_merge($base_parents, array('vertical_tabs'));

        $form['settings'][$type]['vertical_tabs'] = array(
          '#theme_wrappers' => array('vertical_tabs'),
        );
        $settings_form = &$form['settings'][$type]['vertical_tabs'];
      }

      // Iterate through the plugin fields if any exist.
      if ($fields = $this->fieldCallback($type)) {

        foreach ($fields as $name => $info) {

          $parents = $base_parents;
          $settings_group_form = &$settings_form;

          // If the field supports grouping then we'll group them accordantly.
          if (isset($info['group'])) {

            $group_name = $info['group'];
            $parents = array_merge($base_parents, array($group_name));

            if (!isset($settings_form[$group_name])) {
              $settings_form[$group_name] = array(
                '#type' => 'fieldset',
                '#title' => t('@title', array('@title' => drupal_ucfirst($group_name))),
                '#group' => 'vertical_tabs',
              );
            }
            $settings_group_form = &$settings_form[$group_name];
          }
          $values = drupal_array_get_nested_value($settings, $parents);

          // Build the group fields based on the configurations.
          $settings_group_form[$name] = array(
            '#type' => 'textfield',
            '#title' => $info['title'],
            '#required' => isset($info['required']) ? $info['required'] : FALSE,
            '#description' => isset($info['description']) ? $info['description'] : NULL,
            '#field_suffix' => isset($info['field_suffix']) ? $info['field_suffix'] : NULL,
            '#default_value' => isset($values[$name]) ? $values[$name] : $info['default_value'],
          );

          // If the field has a dependencies then we'll attach the form state.
          if (isset($info['dependencies']) && !empty($info['dependencies'])) {
            $settings_group_form[$name]['#states'] = array(
              'visible' => array(
                ':input[name="settings[' . implode('][', array_merge($parents,
                  array($info['dependencies']['field']))) . ']"]' => $info['dependencies']['condition'],
              ),
            );
          }

          // Alter the form element to be a select type if either boolean or
          // have an options array defined and have a type of integer or string.
          if ((isset($info['options']) && ($info['type'] == 'string' ||
            $info['type'] == 'integer')) || $info['type'] == 'boolean') {

            $settings_group_form[$name]['#type'] = 'select';
            $settings_group_form[$name]['#options'] = ($info['type'] == 'boolean') ?
              $this->buildBooleanOptions() : $info['options'];

            // If the field has defined and empty option.
            if (isset($info['empty_option'])) {
              $settings_group_form[$name]['#empty_option'] = $info['empty_option'];
            }
            // If the field has defined and empty value.
            if (isset($info['empty_value'])) {
              $settings_group_form[$name]['#empty_value'] = $info['empty_value'];
            }
          }
          elseif (isset($info['size']) && ($info['type'] == 'string' || $info['type'] == 'integer')) {
            $settings_group_form[$name]['#size'] = $info['size'];
          }
          // Set element validate function if the field has a type of integer.
          if ($info['type'] == 'integer') {
            $settings_group_form[$name]['#element_validate'] = array('tabilicious_validate_integer');
          }
        }
      }

      // Hide the fieldset if the element doesn't have any children.
      if (count(element_children($settings_form)) < 1) {
        $settings_form['#access'] = FALSE;
      }
    }
  }

  /**
   * Format the plugin style parameters.
   *
   * @param $parameters
   *   A nested array of the the parameters to format.
   *
   * @return array
   *   A one dimensional array of all the parameters.
   */
  protected function formatParameters($parameters, &$array = array()) {
    $fields = $this->fieldCallback('parameter');

    // Iterate through the parameters and format them based on the style plugin
    // field type.
    foreach ($parameters as $name => $value) {
      if (is_array($value)) {
        $this->formatParameters($value, $array);
      }
      else {
        // Check to see if the value is not true or false. These are special
        // string values that can be use to define a boolean.
        if ($value != 'true' && $value != 'false') {
          switch ($fields[$name]['type']) {
            case 'boolean':
              $array[$name] = (boolean) check_plain($value);
              break;

            case 'integer':
              $array[$name] = (int) check_plain($value);
              break;

            default:
              $array[$name] = (string) check_plain($value);
              break;
          }
        }
        else {
          $array[$name] = ($value == 'true') ? (boolean) 1 : (boolean) 0;
        }
      }
    }

    return $array;
  }

  /**
   * Build the boolean options.
   *
   * @return array
   *   An array of the boolean options.
   */
  protected function buildBooleanOptions() {
    return array(1 => t('true') , 0 => t('false'));
  }

  /**
   * A validation callback for the operation form.
   *
   * @param $form_state
   *   An array of the form states.
   * @param $settings
   *   An array of the style plugin settings.
   */
  public function validateOperationForm($form_state, $settings) {
    $this->formCallback('operation_validate', array($form_state['op'],
      $form_state, $settings));
  }

  /**
   * A submission callback for the operation form.
   *
   * @param $form_state
   *   An array of the form states.
   * @param $settings
   *   An array of the style plugin settings.
   */
  public function submitOperationForm($form_state, $settings) {
    $this->formCallback('operation_submit', array($form_state['op'],
      $form_state, $settings));
  }

  /**
   * Render the style plugin.
   *
   * @param $selector
   *   A javascript selector ID.
   * @param $items
   *   An array of the items to be rendered.
   * @param $settings
   *   An array of the style plugin settings.
   */
  public function render($selector, $items, $settings) {
    $selector   = tabilicious_html_id($selector);
    $parameters = $this->formatParameters($settings['parameter']);

    // Add the style plugin assets.
    $this->addAssets($settings);

    // Define the settings to set for the tabilicious plugin style.
    $data[$selector] = array(
      'method' => $this->getJsHandler(),
      'parameters' => $parameters,
    );
    drupal_add_js(array('tabilicious' => array('data' => $data)), 'setting');

    $themeing = isset($settings['theming']) ? $settings['theming'] : array();

    // Create the element that will render the tabilicious plugin style.
    $element = array(
      '#items' => $items,
      '#theme' => $this->getTemplate(),
      '#selector' => $selector,
      '#theming' => $themeing,
      '#parameters' => $parameters,
    );
    $element['#attached'] = array(
      'js' => array(
        drupal_get_path('module', 'tabilicious') . '/js/jquery.tabilicious.js' => array(),
      ),
    );

    return $element;
  }

  /**
   * Define the style plugin settings categories.
   */
  protected function categories() {
    return array(
      'parameter' => array(
        'title' => t('Parameter'),
        'collapsed' => FALSE,
        'vertical_tabs' => TRUE,
      ),
      'theming' => array(
        'title' => t('Theming'),
        'collapsed' => TRUE,
        'vertical_tabs' => FALSE,
      ),
    );
  }

  /**
   * Load the style plugins .inc file.
   */
  protected function loadInclude() {
    if (file_exists($this->plugin['path'] . '/' . $this->plugin['file'])) {
      include_once DRUPAL_ROOT . '/' . $this->plugin['path'] . '/' . $this->plugin['file'];
    }
  }

  /**
   * Invoke the style plugins form callback.
   *
   * @param $name
   *   The name of the form to invoke.
   * @param $args
   *   An array of arguments to pass to the callback function.
   */
  protected function formCallback($name, $args = array()) {
    $function = isset($this->plugin['form'][$name]) ?
      $this->plugin['form'][$name] : NULL;

    return $this->callback($function, $name, $args);
  }

  /**
   * Invoke the style plugins field callback.
   *
   * @param $name
   *   The name of the form to invoke.
   * @param $args
   *   An array of arguments to pass to the callback function.
   */
  public function fieldCallback($name, $args = array()) {
    $function = isset($this->plugin['field'][$name]) ?
      $this->plugin['field'][$name] : NULL;

    return $this->callback($function, $name, $args);
  }

  /**
   * Initialize a callback for the provided function.
   *
   * @param $function
   *   The function name to make the callback to.
   * @param $extension
   *   A string that will be appended to make the default callback.
   * @param $args
   *   An array of arguments to pass to the callback function.
   */
  protected function callback($function, $extension, $args) {
    if (!isset($function) || !function_exists($function)) {
      $function = $this->plugin['module'] . '_' . $extension;
      if (!function_exists($function)) {
        return;
      }
    }
    $this->loadInclude();

    return call_user_func_array($function, $args);
  }

  /**
   * Add the plugins JS, CSS, and library assets.
   *
   * @param $settings
   *   An array of the profile plugin settings.
   */
  protected function addAssets($settings) {
    foreach ($this->getAssets() as $type => $asset) {
      if (!is_array($asset) || empty($asset)) {
        continue;
      }
      foreach ($asset as $key => $name) {
        if ($type == 'css' || $type == 'js') {
          call_user_func_array('drupal_add_' . $type, array(
            drupal_get_path('module', $this->plugin['module']) . '/' . $key, $name));
        }
        elseif ($type == 'system') {
          drupal_add_library('system', $name);
        }
        elseif ($type == 'libraries') {
          libraries_load($name);
        }
      }
    }

    // Allow other plugin styles to add assets.
    $this->formCallback('add_assets', array($settings));
  }

  /**
   * Retrieve the style plugins JS handler.
   *
   * @return string
   *   An string that represents the method that needs to be invoked.
   */
  protected function getJsHandler() {
    return isset($this->plugin['handler']['js']) ? $this->plugin['handler']['js'] : NULL;
  }

  /**
   * Retrieve the style plugins assets.
   *
   * @return array
   *   An array of the plugins: JS, CSS, and library assets.
   */
  protected function getAssets() {
    return isset($this->plugin['assets']) ? $this->plugin['assets'] : array();
  }

  /**
   * Retrieve the style plugins template for rendering.
   *
   * @return string
   *   The theme function to use when rendering the element.
   */
  protected function getTemplate() {
    return isset($this->plugin['template']) ? $this->plugin['template'] : 'tabilicious_tabs';
  }
}
